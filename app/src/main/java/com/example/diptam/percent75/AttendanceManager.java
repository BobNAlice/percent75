package com.example.diptam.percent75;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.Arrays;

/**
 * Created by Diptam on 4/10/2016.
 */
public class AttendanceManager {
    DatabaseManager db;
    Attendance attendance;
    Period period;
    Context c;
    String studentlist;
    String present[];
    AttendanceManager(Attendance a, Period p, Context c)
    {
        this.c = c;
        this.period = p;
        this.attendance = a;
    }
    AttendanceManager(Context c)
    {
        this.c =c;
        db = new DatabaseManager(c);
    }

    public boolean saveAttendance()
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", period.getDate());
        contentValues.put("period", period.getPeriodNo());
        contentValues.put("section", period.getSection());
        contentValues.put("department", period.getDepartment());
        contentValues.put("year", period.getYear());
        contentValues.put("t_id",period.getTeachId());
        contentValues.put("record",attendance.makeAttendanceString());
        contentValues.put("Status","0");
        db = new DatabaseManager(c);
        return db.insertAttendance(contentValues);
    }
    public boolean checkAttendance(String dp,String dt,int p,String sc,String tid,int y)
    {
        boolean b=false;
        Cursor c =  db.findAttendanceTaken();
        if (c.moveToFirst())
        { do{
            if(c.getString(0).equalsIgnoreCase(dt) && c.getInt(1)==p && c.getString(2).equalsIgnoreCase(sc) && c.getString(3).equalsIgnoreCase(dp) && c.getInt(4)==y && c.getString(5).equalsIgnoreCase(tid)){
                b=true;
                 break;}
        }while(c.moveToNext());}
        if (c != null && !c.isClosed()) {
            c.close();
        }
        return b;
    }
    /*-- method: databasemanager calling----> pass individuL PERIOD INFO-----*/
    public String[] getAttendance(Period p,String searchdate)
    {
    studentlist = db.getRecord(p, searchdate);
    present = studentlist.split("_");
        return present;}
}
