package com.example.diptam.percent75;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class Batch extends Fragment {
    private View view;
    private CheckBox[] check = new CheckBox[10];
    private Button nextbtn;
    private Button prevbtn;
    private Button save;
    private TextView state;
    public Attendance att = new Attendance();
    public Batch() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      view = inflater.inflate(R.layout.fragment_batch, container, false);
      check[0]= (CheckBox) view.findViewById(R.id.checkBox1);
      check[1]= (CheckBox) view.findViewById(R.id.checkBox2);
      check[2]= (CheckBox) view.findViewById(R.id.checkBox3);
      check[3]= (CheckBox) view.findViewById(R.id.checkBox4);
      check[4]= (CheckBox) view.findViewById(R.id.checkBox5);
      check[5]= (CheckBox) view.findViewById(R.id.checkBox6);
      check[6]= (CheckBox) view.findViewById(R.id.checkBox7);
      check[7]= (CheckBox) view.findViewById(R.id.checkBox8);
      check[8]= (CheckBox) view.findViewById(R.id.checkBox9);
      check[9]= (CheckBox) view.findViewById(R.id.checkBox10);
      state = (TextView) view.findViewById(R.id.textView3);
      save = (Button) view.findViewById(R.id.button5);
      nextbtn = (Button)view.findViewById(R.id.button4);
      prevbtn = (Button)view.findViewById(R.id.button3);
      nextbtn.setOnClickListener(new View.OnClickListener(){

        @Override
        public void onClick(View v) {
          nxtButtonClicked(Integer.parseInt(check[0].getText().toString()),Integer.parseInt(check[9].getText().toString()));
        }
      });
      prevbtn.setOnClickListener(new View.OnClickListener(){

        @Override
        public void onClick(View v) {
          prevButtonClicked(Integer.parseInt(check[0].getText().toString()), Integer.parseInt(check[9].getText().toString()));
        }
      });

        /* ---- Listener setting for check boxes---- */
      setListener();
      save.setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View v) {
          showSaved();
        }
      });
      setValues();
      return view;
    }
  public void setListener()
  {
    for(int i =0;i<10;i++)
    {
      check[i].setOnClickListener(new View.OnClickListener(){

        @Override
        public void onClick(View v)
        {
          CheckBox ch = (CheckBox)v;
          setMark(Integer.parseInt(ch.getText().toString()), ch.isChecked());
        }
      });
    }
  }
  public void setValues()
  {   Integer start=new Student(getActivity()).getFirstStudent(new Period(getActivity())),end=start+9,temp =new Student(getActivity()).getFirstStudent(new Period(getActivity())) ;

    for(int i = 0; i< 10; i++)
    {
      check[i].setText(temp.toString());
      check[i].setChecked(att.getCurrentAttendance(temp));
      temp++;
    }
    state.setText( start + " to " + end);

  }
  public void nxtButtonClicked(int start, int end){
    Integer temp = new Integer(start+10);
    if(end==new Student(getActivity()).getLastStudent(new Period(getActivity())))
    {
      android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
      alertDialogBuilder.setMessage("This is Last Batch");
      alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int arg1) {
        }
      });
      android.app.AlertDialog alertDialog = alertDialogBuilder.create();
      alertDialog.show();
    }
    else{
    for(int i = 0; i< 10; i++)
    {
      check[i].setText(temp.toString());
      check[i].setChecked(att.getCurrentAttendance(temp));
      temp++;
      if(i==new Student(getActivity()).getLastStudent(new Period(getActivity())))
        break;
    }
    start= start+10;
    end = end+10;
    state.setText(start + " to " + end);}

  }
  public void setMark(int i, boolean b)
  {
    att.setCuurentAttendance(i, b);
  }
  public void prevButtonClicked(int start, int end){
    Integer temp = new Integer(start-10);
    if(start==new Student(getActivity()).getFirstStudent(new Period(getActivity())))
    {
      android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
      alertDialogBuilder.setMessage("This is First Batch");
      alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int arg1) {
        }
      });
      android.app.AlertDialog alertDialog = alertDialogBuilder.create();
      alertDialog.show();
    }
    else{
    if(start>1){
      for(int i = 0; i< 10; i++)
      {
        check[i].setText(temp.toString());
        check[i].setChecked(att.getCurrentAttendance(temp));
        temp++;
      }
      start= start-10;
      end = end-10;
      state.setText( start + " to " + end);
    }}
  }
  public void showSaved()
  {
    AttendanceManager am = new AttendanceManager( att,new Period(getActivity()), getActivity());
    if(am.saveAttendance())
      Toast.makeText(getActivity().getApplicationContext(), "Saved Successfully", Toast.LENGTH_SHORT).show();
    else
      Toast.makeText(getActivity().getApplicationContext(),"Something went Wrong", Toast.LENGTH_SHORT).show();

  }

}
