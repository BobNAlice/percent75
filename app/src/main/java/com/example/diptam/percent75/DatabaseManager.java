package com.example.diptam.percent75;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
/**
 * Created by Diptam on 4/10/2016.
 */
public class   DatabaseManager extends SQLiteAssetHelper {
    private static String DB_NAME= "datastore.db";
    private  static int DB_VER = 1;
    public DatabaseManager(Context context) {
        super(context, DB_NAME, null , DB_VER);
    }

    public Cursor getUser() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String [] sqlSelect = {"0 _id","id", "password","name"};
        String sqlTables = "login";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, null, null,
                null, null, null);
        c.moveToFirst();
        return c;
    }

    public Cursor getRoutine() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String [] sqlSelect = new String[]{"0 _id", "per","year","section","department","day","t_id","sub"};
        String sqlTables = "routine";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, null, null,
                null, null, null);
        c.moveToFirst();
        return c;
    }

    public boolean insertAttendance(ContentValues c)
    {
        SQLiteDatabase db = getWritableDatabase();
        long rowID = db.insert("attendance", null, c);
        db.close();
        if(rowID < 0)
            return false;
        else return true;
    }
/* RECEIVE INDIVIDUAL PERIOD INFO----> FETCH CORRESPONDING DATA----> RETURN */
    public String getRecord(Period p,String searchdate)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor res =  db.rawQuery("select record from attendance where period=" + p.getPeriodNo() + " and year=" + p.getYear() + " and date=? and department=? and section=?", new String[]{searchdate, p.getDepartment(), p.getSection()});
        res.moveToFirst();
        return res.getString(0);
    }
    public Cursor findAttendanceTaken()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor res =  db.rawQuery("select date,period,section,department,year,t_id from attendance",null);
        res.moveToFirst();
        return res;
    }
  public Cursor getStudent(Period p)
  {
      SQLiteDatabase db = getReadableDatabase();
      Cursor res =  db.rawQuery("select roll,name from stu where year="+p.getYear()+" and department=? and section=?",new String[]{p.getDepartment(),p.getSection()});
      res.moveToFirst();
      return res;
  }
    public Cursor getFirstRoll(Period p)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor res =  db.rawQuery("select roll from stu where year="+p.getYear()+" and department=? and section=?",new String[]{p.getDepartment(),p.getSection()});
        res.moveToFirst();
        return res;
    }
    public Cursor getLastRoll(Period p)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor res =  db.rawQuery("select roll from stu where year="+p.getYear()+" and department=? and section=?",new String[]{p.getDepartment(),p.getSection()});
        res.moveToLast();
        return res;
    }


    public Cursor getAttendanceUpdate(){

        SQLiteDatabase db1 = getReadableDatabase();
        Cursor res =  db1.rawQuery( "select * from attendance where Status=? ", new String[]{"0"});
        res.moveToFirst();

        return res;

    }
    /*---- write a data to update status ----*/
    public boolean setStatus()
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("Status", "1");
        return db.update("attendance",args, " Status=?", new String[]{"0"})>0;

    }

}
