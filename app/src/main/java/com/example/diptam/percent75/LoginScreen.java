package com.example.diptam.percent75;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginScreen extends AppCompatActivity {

    String id , pss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
    }

    public void logIn(View v)
    {

        final Intent i = new Intent(this,PeriodScreen.class);
        Toast.makeText(getApplicationContext(), "Button Clicked", Toast.LENGTH_SHORT).show();

        EditText uname = (EditText)findViewById(R.id.editText);
        EditText pass = (EditText)findViewById(R.id.editText2);


        String unm = uname.getText().toString();
        String psswrd= pass.getText().toString();
        User u=new User(unm,psswrd,this);
        Cursor c =  u.validateUser();

        do{
            id=c.getString(1);
            pss=c.getString(2);
            if(id.equalsIgnoreCase(unm) && !pss.equalsIgnoreCase(psswrd))
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Please Enter Valid Password");
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }


            if(id.equalsIgnoreCase(unm) && pss.equalsIgnoreCase(psswrd))
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Welcome"+" "+c.getString(3));
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        i.putExtra("message", id);
                        startActivity(i);

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
            }


        }while(c.moveToNext());

        if(!id.equalsIgnoreCase(unm) && !pss.equalsIgnoreCase(psswrd))

        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Please Enter Valid Log in Information");
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {


                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();



        }

    }
}
