package com.example.diptam.percent75;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by Diptam on 4/10/2016.
 */
public class Period {
    private static String SECTION;
    private static String DEPT;
    private static int YEAR;
    private static int PERNO;
    private static String TEACHID;
    private static String DATE;
    public DatabaseManager db;
    Period(Context c)
    {
        db = new DatabaseManager(c);
    }
    Cursor validateRoutine()
    {
        return db.getRoutine();
    }

    /*------------------ SETTER METHODS---------*/
    public void setDate(String s) {this.DATE = s;}
    public void setSection(String s)
    {
        this.SECTION = s;
    }
    public void setDepartment(String s)
    {
        this.DEPT = s;
    }
    public void setYear(int s)
    {
        this.YEAR= s;
    }
    public void setPeriodNo(int s)
    {
        this.PERNO=s;
    }
    public void setTeachId(String s)
    {
        this.TEACHID =s;
    }
    /*------------------ GETTER METHOD--------------------*/
    public String getDate() {return this.DATE ;}
    public String getSection()
    {
        return this.SECTION;
    }
    public String getDepartment()
    {
        return this.DEPT;
    }
    public int getYear()
    {
        return this.YEAR;
    }
    public int getPeriodNo()
    {
        return this.PERNO;
    }
    public String getTeachId()
    {
        return this.TEACHID;
    }
}
