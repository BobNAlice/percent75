package com.example.diptam.percent75;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PeriodScreen extends AppCompatActivity {

    public Cursor c;
    public Period rt;
    RadioButton r1,r2,r3,r4,r5,r6,r7,r8,r9;
    EditText per;
    Button start,preview;
    int btnclck[]=new int[10];
    int flag[]=new int[10];
    TextView sub;
    int p,y,pr;
    String sc,dp,dy,tid,sb,prr,message,sd,todaydate;
    SimpleDateFormat dayFormat,dtformat;
    Calendar ca;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_period_screen);
        r1 = (RadioButton) findViewById(R.id.radioButton);
        r2 = (RadioButton) findViewById(R.id.radioButton2);
        r3 = (RadioButton) findViewById(R.id.radioButton3);
        r4 = (RadioButton) findViewById(R.id.radioButton4);
        r5 = (RadioButton) findViewById(R.id.radioButton5);
        r6 = (RadioButton) findViewById(R.id.radioButton6);
        r7 = (RadioButton) findViewById(R.id.radioButton7);
        r8 = (RadioButton) findViewById(R.id.radioButton8);
        r9 = (RadioButton) findViewById(R.id.radioButton9);
        per = (EditText) findViewById(R.id.editText3);
        sub = (TextView) findViewById(R.id.textView7);
        start = (Button) findViewById(R.id.button3);
        start.setEnabled(false);
        preview = (Button) findViewById(R.id.button8);
        preview.setEnabled(false);

    }
public void onResume()
{
    super.onResume();
    start.setEnabled(false);

}
    public void startAttendance(View v)
    {
        boolean b=new AttendanceManager(this).checkAttendance(dp,todaydate,p,sc,tid,y);
        if(b)
        {
           /* if(y==4)
            {

            }
            else{*/
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("This Attendance Already Taken");
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
       // }
        }
        else{
            if(y==4) btnclck[p]+=1;
        rt.setDepartment(dp);
        rt.setDate(todaydate);
        rt.setPeriodNo(p);
        rt.setSection(sc);
        rt.setTeachId(tid);
        rt.setYear(y);
        Intent i = new Intent(this,AttendanceScreen.class);
        startActivity(i);}
    }
     public void showPreview(View v)
     {

         final Intent i = new Intent(this,PreviewScreen.class);
         AlertDialog.Builder alert = new AlertDialog.Builder(this);
         alert.setTitle("Confirm Date To Preview");
         alert.setMessage("Enter Date To Change");
         final EditText inputdate = new EditText(PeriodScreen.this);
       LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
               LinearLayout.LayoutParams.MATCH_PARENT,
               LinearLayout.LayoutParams.MATCH_PARENT);
         inputdate.setLayoutParams(lp);
         alert.setView(inputdate);
         inputdate.setText(todaydate);

             alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int whichButton) {
                     String srt = inputdate.getEditableText().toString();
                     i.putExtra("searchdate", srt);
                     startActivity(i);
                 }
             });
             AlertDialog alertDialog = alert.create();
             alertDialog.show();

       //  Intent i = new Intent(this,PreviewScreen.class);
       //  startActivity(i);


     }

    public void showDetails(View v) {
     /*   start.setEnabled(true);
       preview.setEnabled(true);*/
        Toast.makeText(getApplicationContext(), "Clicked", Toast.LENGTH_SHORT).show();
        rt = new Period(this);
        c = rt.validateRoutine();
        Bundle bundle = getIntent().getExtras();
        message = bundle.getString("message");
        dayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        ca = Calendar.getInstance();
        sd = dayFormat.format(ca.getTime());
        dtformat= new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        todaydate = dtformat.format(ca.getTime());

        prr = per.getText().toString();

        String regex = "-?\\d+(\\.\\d+)?";
        if (prr.matches(regex))  {
            pr = Integer.parseInt(prr);
            if (pr > 7 || pr < 1) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Please Enter Period No Between 1-7");
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            else if (pr == 4) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Break Time");
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

            else if(pr>0 && pr<8 && pr!=4)
            {
                do {
                    p = c.getInt(1);
                    y = c.getInt(2);
                    sc = c.getString(3);
                    dp = c.getString(4);
                    dy = c.getString(5);
                    tid = c.getString(6);
                    sb = c.getString(7);

                    if (dy.equalsIgnoreCase(sd) && tid.equalsIgnoreCase(message) && p == pr) {
                        if(y==4 && btnclck[pr]==1 && flag[pr]==0)
                        {
                            flag[pr]=1;
                            continue;
                        }
                        else{
                            if (dp.equalsIgnoreCase("cse")) {
                                r1.setChecked(true);
                                rt.setDepartment("cse");
                            }
                            if (dp.equalsIgnoreCase("it")) {
                                r2.setChecked(true);
                                rt.setDepartment("it");
                            }
                            if (dp.equalsIgnoreCase("ece")) {
                                r3.setChecked(true);
                                rt.setDepartment("ece");
                            }
                            if (y == 1) {
                                r4.setChecked(true);
                                rt.setYear(1);
                            }
                            if (y == 2) {
                                r5.setChecked(true);
                                rt.setYear(2);
                            }
                            if (y == 3) {
                                r6.setChecked(true);
                                rt.setYear(3);
                            }
                            if (y == 4) {
                                r7.setChecked(true);
                                rt.setYear(4);
                            }
                            if (sc.equalsIgnoreCase("a")) {
                                r8.setChecked(true);
                                rt.setSection("a");
                            }
                            if (sc.equalsIgnoreCase("b")) {
                                r9.setChecked(true);
                                rt.setSection("b");
                            }
                            sub.setText(sb);
                            rt.setPeriodNo(pr);
                            rt.setTeachId(tid);
                            start.setEnabled(true);
                            preview.setEnabled(true);
                      /*  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                        alertDialogBuilder.setMessage("Please Click Preview to View Taken Attendance\nPlease Click Start To Take Attendance");
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();*/
                            break;
                        }
                    }

                } while (c.moveToNext());
                if (dy.equalsIgnoreCase(sd) && tid.equalsIgnoreCase(message) && p != pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("This Period Is Not Yours");
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else if (!dy.equalsIgnoreCase(sd) && tid.equalsIgnoreCase(message) && p == pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("You Have no class on "+sd);
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
               else if (dy.equalsIgnoreCase(sd) && !tid.equalsIgnoreCase(message) && p == pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("Period"+pr+" Is Not Yours");
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else if (!dy.equalsIgnoreCase(sd) && tid.equalsIgnoreCase(message) && p != pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("You Have no class on "+sd+" in period"+pr);
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else if (!dy.equalsIgnoreCase(sd) && !tid.equalsIgnoreCase(message) && p == pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("Hello "+message+" You Have no class on "+sd);
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else if (dy.equalsIgnoreCase(sd) && !tid.equalsIgnoreCase(message) && p != pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("Hello "+message+" You Have no class on "+sd+" in period "+pr);
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                else if(!dy.equalsIgnoreCase(sd) && !tid.equalsIgnoreCase(message) && p != pr)
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("No period match");
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {


                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }}
        else
        {
            if(prr.trim().length()== 0)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Please Enter Period No");
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

            else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Please Enter Valid Period No");
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }}
    }
}
