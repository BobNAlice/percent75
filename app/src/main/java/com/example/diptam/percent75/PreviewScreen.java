package com.example.diptam.percent75;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;

import java.util.Arrays;

public class PreviewScreen extends AppCompatActivity {
   // DatabaseManager dm;
    AttendanceManager am;
    Student st;
    String stud[]=new String[100];
    String present[]=null;
    String studentlist,searchdate;
    TableLayout tl;
    TableRow tr;
    TextView name,mark;
    Button b;
    EditText text1;
    int i=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        //dm= new DatabaseManager(this);
        Bundle bundle = getIntent().getExtras();
        searchdate = bundle.getString("searchdate");
        am=new AttendanceManager(this);
        tl = (TableLayout) findViewById(R.id.maintable);
        present = am.getAttendance(new Period(this), searchdate);
        b = (Button) findViewById(R.id.button7);
        text1=(EditText)findViewById(R.id.editText4);


        st=new Student(this);
        stud=st.getStudentInfo(new Period(this),present);
        // studentlist= dm.getRecord(new Period(this));
        // studentlist= am.getAttendance(new Period(this));
        // present=studentlist.split(",");
        /*Cursor c =  dm.getStudent(new Period(this));
        if (c.moveToFirst())
        { do{
            if(Arrays.asList(present).contains(c.getString(0))){
            stud[i]=c.getString(1);
            i++;}
        }while(c.moveToNext());}
        if (c != null && !c.isClosed()) {
            c.close();
        }*/

        addHeaders();
        addData();}
        catch(Exception e){
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Today No Attendance Taken");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {


                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }
   /* public void abc(View v)
    {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(st.getLastStudent(new Period(this))+""+st.getFirstStudent(new Period(this)));

        alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {


            }
        });

        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }*/
    public void Click(View v) throws JSONException {

        UpdateManager up=new UpdateManager(this);
        up.UpdateServer(text1.getText().toString());
    }

    public void addHeaders(){

        /** Create a TableRow dynamically **/
        tr = new TableRow(this);
        tr.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.FILL_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT));
        TextView mark = new TextView(this);
        mark.setText("ROLL NO");
        //name.setTextSize(50);
        mark.setTextColor(Color.GRAY);
        mark.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        mark.setPadding(5, 5, 5, 0);
        mark.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(mark);
        TextView name = new TextView(this);
        name.setText("NAME");
       // name.setTextSize(50);
        name.setTextColor(Color.GRAY);
        name.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        name.setPadding(5, 5, 5, 0);
        name.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(name);// Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        // we are adding two textviews for the divider because we have two columns
        tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        /** Creating another textview **/
        TextView divider = new TextView(this);
        divider.setText("-----------------");
        divider.setTextColor(Color.WHITE);
        divider.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        divider.setPadding(5, 0, 0, 0);
        divider.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider); // Adding textView to tablerow.

        TextView divider2 = new TextView(this);
        divider2.setText("-------------------------");
        divider2.setTextColor(Color.WHITE);
        divider2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        divider2.setPadding(5, 0, 0, 0);
        divider2.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider2); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
    }
    public void addData(){
    for (int i = 0; i < present.length; i++) {
        /** Create a TableRow dynamically **/
        tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        /** Creating a TextView to add to the row **/
          /*  name = new TextView(this);
            name.setText(stud[i]);
            name.setTextColor(Color.RED);
            name.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            name.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            name.setPadding(5, 5, 5, 5);
            tr.addView(name);  // Adding textView to tablerow.*/

        /** Creating another textview **/
        mark = new TextView(this);
        mark.setText(present[i]);
        mark.setTextColor(Color.WHITE);
        mark.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        mark.setPadding(5, 5, 5, 5);
        mark.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(mark); // Adding textView to tablerow.
        name = new TextView(this);
        name.setText(stud[i]);
        name.setTextColor(Color.WHITE);
        name.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        name.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        name.setPadding(5, 5, 5, 5);
        tr.addView(name);

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
    }

}
}
