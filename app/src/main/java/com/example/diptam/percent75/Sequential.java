package com.example.diptam.percent75;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sequential extends Fragment {

    private View view;
    private Button nextBtn;
    private Button prevBtn;
    private Button save;
    private TextView numText;
    public Attendance  at = new Attendance();
    //public Student st;
    public CheckBox ch;
    //public Period p;

    public Sequential() {
        // Required empty public constructor
      //  p=new Period(getActivity());
       //st=new Student(getActivity());
       // st=new Student(getActivity());
//p
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_sequential, container, false);
        nextBtn = (Button) view.findViewById(R.id.button2);
        prevBtn = (Button) view.findViewById(R.id.button);
        numText = (TextView) view.findViewById(R.id.textView);
        save = (Button) view.findViewById(R.id.button6);
        ch = (CheckBox) view.findViewById(R.id.markMe);
        at.setDefaultAttendance(true, new Period(getActivity()),new Student(getActivity()));
        showStatus(new Student(getActivity()).getFirstStudent(new Period(getActivity())));
        numText.setText(new Student(getActivity()).getFirstStudent(new Period(getActivity())) + "");
        nextBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nextBtnClick();
            }
        });
        prevBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                prevBtnClick();
            }
        });
        ch.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                setMark();
            }
        });
        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showSaved();
            }
        });
        return view;
    }

    public void nextBtnClick()
    {
        if(Integer.parseInt(numText.getText().toString())==new Student(getActivity()).getLastStudent(new Period(getActivity()))) {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("This is the last roll");
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        else{
        Integer i = new Integer(Integer.parseInt(numText.getText().toString())+1);
        showStatus(i);
        numText.setText(i.toString());}
    }
    public void prevBtnClick()
    {
        if(Integer.parseInt(numText.getText().toString())==new Student(getActivity()).getFirstStudent(new Period(getActivity())))
        {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("This is First Roll");
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        else{
        Integer i = new Integer(Integer.parseInt(numText.getText().toString())-1);
        showStatus(i);
        if (i>0) numText.setText(i.toString());}
    }
    public void showStatus(int i)
    {
        Boolean b = at.getCurrentAttendance(i);
        if(b.equals(true))
            ch.setText("Present");
        else
            ch.setText("Absent");
        ch.setChecked(b);
    }
    public void setMark()
    {
        Integer i = new Integer(Integer.parseInt(numText.getText().toString()));
        at.setCuurentAttendance(i, ch.isChecked());
        showStatus(i);
    }

    public void showSaved()
    {
       AttendanceManager am = new AttendanceManager( at,new Period(getActivity()), getActivity());
        if(am.saveAttendance())
          Toast.makeText(getActivity().getApplicationContext(), "Saved Successfully", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getActivity().getApplicationContext(),"Something went Wrong", Toast.LENGTH_SHORT).show();

    }

}
