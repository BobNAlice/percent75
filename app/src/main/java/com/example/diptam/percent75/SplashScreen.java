package com.example.diptam.percent75;

import android.app.Activity;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.*;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class SplashScreen extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView img_animation = (ImageView) findViewById(R.id.imgLogo);
        TranslateAnimation animation = new TranslateAnimation(0.0f, 100.0f,0.0f, 0.0f);
        animation.setDuration(1000);
        animation.setRepeatCount(5);
        animation.setRepeatMode(2);
        animation.setFillAfter(true);
        img_animation.startAnimation(animation);
         installationDbLoading();
      /*  File file1=new File(Environment.getExternalStorageDirectory().getPath()+"/Percent75/");
        if(!file1.exists()){
        file1.mkdir();}
        File file=new File(Environment.getExternalStorageDirectory().getPath()+"/Percent75/datastore.db");
        if(!file.exists()) {


            OutputStream output;
            AssetManager assetManager = getAssets();
            InputStream in;
            try {
                output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/Percent75/datastore.db");
                in = assetManager.open("databases/datastore.db");
                byte data[] = new byte[1024];

                long total = 0;
                int count;
                while ((count = in.read(data)) != -1) {


                    // writing data to file
                    output.write(data, 0, count);
                }
                //  output.flush(); output.close();
                in.close();
                Toast.makeText(getApplicationContext(), "copied", Toast.LENGTH_SHORT).show();
                // closing streams

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), Environment.getExternalStorageDirectory().getPath()+"something wrong", Toast.LENGTH_SHORT).show();
            }
            try{
                SQLiteDatabase db = SQLiteDatabase.openDatabase(Environment.getExternalStorageDirectory().getPath() + "/Percent75/datastore.db", null, 0);
                Cursor res = db.rawQuery("select * from student ", null);
                res.moveToFirst();
                Toast.makeText(getApplicationContext(),res.getString(0), Toast.LENGTH_SHORT).show();
                db.close();
            }catch(Exception e){ Toast.makeText(getApplicationContext(),e.getMessage()+"", Toast.LENGTH_SHORT).show();}

        }*/

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, LoginScreen.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);



    }
    public void installationDbLoading()
    {
        File file1=new File(Environment.getExternalStorageDirectory().getPath()+"/Percent75/");
        if(!file1.exists()){
            file1.mkdir();}
        File file=new File(Environment.getExternalStorageDirectory().getPath()+"/Percent75/datastore.db");
        if(!file.exists()) {


            OutputStream output;
            AssetManager assetManager = getAssets();
            InputStream in;
            try {
                output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/Percent75/datastore.db");
                in = assetManager.open("databases/datastore.db");
                byte data[] = new byte[1024];

                long total = 0;
                int count;
                while ((count = in.read(data)) != -1) {


                    // writing data to file
                    output.write(data, 0, count);
                }
                //  output.flush(); output.close();
                in.close();
                Toast.makeText(getApplicationContext(), "copied", Toast.LENGTH_SHORT).show();
                // closing streams

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), Environment.getExternalStorageDirectory().getPath()+"something wrong", Toast.LENGTH_SHORT).show();
            }
           /* try{
                SQLiteDatabase db = SQLiteDatabase.openDatabase(Environment.getExternalStorageDirectory().getPath() + "/Percent75/datastore.db", null, 0);
                Cursor res = db.rawQuery("select * from student ", null);
                res.moveToFirst();
                Toast.makeText(getApplicationContext(),res.getString(0), Toast.LENGTH_SHORT).show();
                db.close();
            }catch(Exception e){ Toast.makeText(getApplicationContext(),e.getMessage()+"", Toast.LENGTH_SHORT).show();}*/

        }
    }

}