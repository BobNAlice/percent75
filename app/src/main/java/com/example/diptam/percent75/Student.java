package com.example.diptam.percent75;

import android.content.Context;
import android.database.Cursor;

import java.util.Arrays;
/**
 * Created by Personal on 11-04-2016.
 */
public class Student {
    private static String SECTION;
    private static String DEPT;
    private static int YEAR;
    private static int ROLL;
    private static String NAME;
    private static String stud[];
    public DatabaseManager db;
    private static int frll=0,lrll=0;

    Student(Context c)
    {
        db = new DatabaseManager(c);
    }
    public String[] getStudentInfo(Period p,String present[])
    {
        int i=0;
        stud=new String[100];
        Cursor c =  db.getStudent(p);
        if (c.moveToFirst())
        { do{
            if(Arrays.asList(present).contains(c.getString(0))){
                stud[i]=c.getString(1);
                i++;}
        }while(c.moveToNext());}
        if (c != null && !c.isClosed()) {
            c.close();
        }
        return stud;
    }
    public int getFirstStudent(Period p)
    {
        Cursor c=db.getFirstRoll(p);
        String rl=c.getString(0);
        String regex = "-?\\d+(\\.\\d+)?";
        if (rl.matches(regex))             frll = Integer.parseInt(rl);
        return frll;
    }
    public int getLastStudent(Period p)
    {
        Cursor c=db.getLastRoll(p);
        String rl=c.getString(0);
        String regex = "-?\\d+(\\.\\d+)?";
        if (rl.matches(regex))             lrll = Integer.parseInt(rl);
        return lrll;
    }
    public void setSection(String s) {this.SECTION = s;}
    public void setDepartment(String s) {this.DEPT = s;}
    public void setYear(int s) {this.YEAR= s;}
    public void setRoll(int s) {this.ROLL=s;}
    public void setName(String s) {this.NAME =s;}
    public String getSection() {return this.SECTION;}
    public String getDepartment() {return this.DEPT;}
    public int getYear() {return this.YEAR;}
    public int getRoll() {return this.ROLL;}
    public String getName() {return this.NAME;}
}

