package com.example.diptam.percent75;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by Diptam on 4/10/2016.
 */
public class User {
    private String USER_NAME;
    private String USER_PASSWORD;
    public DatabaseManager db;
    User(String a, String b, Context c)
    {
        this.USER_NAME =a;
        this.USER_PASSWORD= b;
        db = new DatabaseManager(c);
    }
     Cursor validateUser()
     {
         return db.getUser();
     }
}
